import {
    SendTodoEvent,
    getAlllist,
    getAllActive,
    getAllComplete,
    deletecomplete,
    completeAll,
} from "./ajax.js";
import { removeGetAlllisterner } from "./EventClean.js";

(function (window) {
    "use strict";
    // Your starting point. Enjoy the ride!
    //action on input todo
    $("#new-todo").keypress(function (event) {
        SendTodoEvent(event);
    });

    $("#clear-completed").click(function () {
        deletecomplete();
        removeGetAlllisterner();
        getAlllist();
    });

    $("#getAll").click(function () {
        location.reload();
    });
    $("#getActive").click(function () {
        removeGetAlllisterner();
        getAllActive();
    });

    $("#getCompleted").click(function () {
        removeGetAlllisterner();
        getAllComplete();
    });
    $("#toggle-all").click(function () {
        completeAll();
        removeGetAlllisterner();
        getAlllist();
    });
    //get all element
    getAlllist();
})(window);
