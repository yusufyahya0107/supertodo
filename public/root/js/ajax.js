import { removeGetAlllisterner } from "./EventClean.js";
var RootUrl = "http://127.0.0.1:8000";

export function SendTodoEvent(event) {
    const settings = {
        url: RootUrl+"/api",
        method: "POST",
        timeout: 0,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
            content: event.target.value,
            active: 1,
        },
    };
    const keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
        $.ajax(settings).done(function (response) {
        });
        removeGetAlllisterner();
        getAlllist();
    }
}

function EnterPress(event, id, active) {
    const keycode = event.keyCode ? event.keyCode : event.which;
    if (keycode == "13") {
        if (event.target.value) {
            changeTodolabel(id, event.target.value);
            $(`#${id}view`).text(event.target.value);
        }
        // ganti kelas
        if(active){
        $(`#${id}li`).removeClass("editing").addClass("view");
        }else{
            $(`#${id}li`).removeClass("editing").addClass("completed");
        }
    }
}
// mengabil seluruh data
export async function getAlllist() {
    const element = [];
    $("#list-show").empty();
    const settings = {
        url: RootUrl+"/api",
        method: "GET",
        timeout: 0,
    };

    const response = await $.ajax(settings);
    const datas = await response.data;
    if(datas.length ===0){
        $('#mainsection').css('visibility', 'hidden');
    }
    if(datas.length >0){
        $('#mainsection').css('visibility', 'visible');
    }
    datas.map((data, index) => {
        element.push(`<li id=${data.id}li class=${
            data.active ? "view" : "completed"
        }>
		<div class="view">
			<input id=${data.id}toogle class="toggle" type="checkbox" ${data.active?null:"checked"} />
			<label id=${data.id}view >${data.content}</label>
			<button id=${data.id}destroy class="destroy"></button>
		</div>
		<input id=${data.id}edit class="edit" />
	</li>`);
        $(document).on("click", `#${data.id}view`, function () {
            $(`#${data.id}li`).removeClass("view").addClass("editing");
        });
        $(document).on("click", `#${data.id}destroy`, function () {
            hapus(data.id);
            $(`#${data.id}li`).remove();
        });
        $(document).on("keypress", `#${data.id}edit`, function (event) {
            EnterPress(event, data.id, data.active);
            data.content = event.target.value;
        });
		$(document).on("click", `#${data.id}toogle`, function () {
			if($(`#${data.id}li`).hasClass('view')){
			changeToComplete(data.id)
			$(`#${data.id}li`).removeClass("view").addClass("completed");
			}else{
				changeToActive(data.id)
				$(`#${data.id}li`).removeClass("completed").addClass("view");
			}
        });
    });

    $("#list-show").append(element);
    const count = await countAll();
    $("#count").text(count);
}

// mengambil seluruh data yang saja complete
export async function getAllComplete() {
    const element = [];
    $("#list-show").empty();
    const settings = {
        "url": RootUrl+"/api/complete",
        "method": "GET",
        "timeout": 0,
      };

    const response = await $.ajax(settings);
    const datas = await response.data;
    datas.map((data, index) => {
        element.push(`<li id=${data.id}li class=${
            data.active ? "view" : "completed"
        }>
		<div class="view">
			<input id=${data.id}toogle class="toggle" type="checkbox" ${data.active?null:"checked"} />
			<label id=${data.id}view >${data.content}</label>
			<button id=${data.id}destroy class="destroy"></button>
		</div>
		<input id=${data.id}edit class="edit" />
	</li>`);
        $(document).on("click", `#${data.id}view`, function () {
            $(`#${data.id}li`).removeClass("view").addClass("editing");
        });
        $(document).on("click", `#${data.id}destroy`, function () {
            hapus(data.id);
            $(`#${data.id}li`).remove();
        });
        $(document).on("keypress", `#${data.id}edit`, function (event) {
            EnterPress(event, data.id);
            data.content = event.target.value;
        });
		$(document).on("click", `#${data.id}toogle`, function () {
			if($(`#${data.id}li`).hasClass('view')){
			changeToComplete(data.id)
			$(`#${data.id}li`).removeClass("view").addClass("completed");
			}else{
				changeToActive(data.id)
				$(`#${data.id}li`).removeClass("completed").addClass("view");
                $(`#${data.id}li`).remove();
			}
        });
    });

    $("#list-show").append(element);
    const count = await countComplete();
    $("#count").text(count);
}

// mengambil selurh data yang aktif saja
export async function getAllActive() {
    const element = [];
    $("#list-show").empty();
    const settings = {
        "url": RootUrl+"/api/active",
        "method": "GET",
        "timeout": 0,
      };

    const response = await $.ajax(settings);
    const datas = await response.data;
    datas.map((data, index) => {
        element.push(`<li id=${data.id}li class=${
            data.active ? "view" : "completed"
        }>
		<div class="view">
			<input id=${data.id}toogle class="toggle" type="checkbox" ${data.active?null:"checked"} />
			<label id=${data.id}view >${data.content}</label>
			<button id=${data.id}destroy class="destroy"></button>
		</div>
		<input id=${data.id}edit class="edit" />
	</li>`);
        $(document).on("click", `#${data.id}view`, function () {
            $(`#${data.id}li`).removeClass("view").addClass("editing");
        });
        $(document).on("click", `#${data.id}destroy`, function () {
            hapus(data.id);
            $(`#${data.id}li`).remove();
        });
        $(document).on("keypress", `#${data.id}edit`, function (event) {
            EnterPress(event, data.id);
            data.content = event.target.value;
        });
		$(document).on("click", `#${data.id}toogle`, function () {
			if($(`#${data.id}li`).hasClass('view')){
			changeToComplete(data.id)
			$(`#${data.id}li`).removeClass("view").addClass("completed");
            $(`#${data.id}li`).remove();
			}else{
				changeToActive(data.id)
				$(`#${data.id}li`).removeClass("completed").addClass("view");
			}
        });
    });

    $("#list-show").append(element);
    const count = await countActive();
    $("#count").text(count);
}
// menghapus data
function hapus(id) {
    const settings = {
        url: RootUrl+"/api",
        method: "DELETE",
        timeout: 0,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Cookie: "csrftoken=rlS0EB8zcZsHuuRYkT1C8enN436sBtBx0jQPdmTrrubRQZCVK3whyL6u6ZNqXWcq",
        },
        data: {
            id: id,
        },
    };

    $.ajax(settings).done(function (response) {

    });
}
// mengubah label activitas
function changeTodolabel(id, change) {
    var settings = {
        url: RootUrl+"/api",
        method: "PUT",
        timeout: 0,
        data: {
            id: id,
            content: change,
        },
    };

    $.ajax(settings).done(function (response) {
    });
}
// mengubah status jadi active
function changeToActive(id){
	var settings = {
		"url": RootUrl+"/api/active",
		"method": "PUT",
		"timeout": 0,
		"data": {
		  "id": id
		}
	  };
	  
	  $.ajax(settings).done(function (response) {
	  });
}
// mengubah status jadi complete
function changeToComplete(id){
	var settings = {
		"url": RootUrl+"/api/complete",
		"method": "PUT",
		"timeout": 0,
		"headers": {
		  "Content-Type": "application/x-www-form-urlencoded",
		  "Cookie": "csrftoken=rlS0EB8zcZsHuuRYkT1C8enN436sBtBx0jQPdmTrrubRQZCVK3whyL6u6ZNqXWcq"
		},
		"data": {
		  "id": id
		}
	  };
	  
	  $.ajax(settings).done(function (response) {
	  });
}
// menghitung seluruh data
async function countAll(){
    const settings = {
        "url": RootUrl+"/api/all",
        "method": "GET",
        "timeout": 0,
      };
      
      const response= await $.ajax(settings);
      const data = response.count;
      return data;
}

// menghitung seluruh data active
async function countActive(){
    const settings = {
        "url": RootUrl+"/api/activecount",
        "method": "GET",
        "timeout": 0,
      };
      
      const response= await $.ajax(settings);
      const data = response.count;
      return data;
}
// menghitung seluruh data complete
async function countComplete(){
    const settings = {
        "url": RootUrl+"/api/completecount",
        "method": "GET",
        "timeout": 0,
      };
      
      const response= await $.ajax(settings);
      const data = response.count;
      return data;
}

// membuat seluruh activitas complete
export function completeAll(){
    const settings = {
        "url": RootUrl+"/api/completeall",
        "method": "PUT",
        "timeout": 0,
      };
      
      $.ajax(settings)
}
// menghapus seluruh complete
export function deletecomplete(){
    var settings = {
        "url": RootUrl+"/api/deletecomplete",
        "method": "DELETE",
        "timeout": 0,
      };
      
      $.ajax(settings).done(function (response) {
      });

}