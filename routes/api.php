<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TodoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::get('l', function () {
    return "i love you laravel";
});
Route::get('/',[TodoController::class, 'index']);
Route::get('activecount',[TodoController::class, 'activeCount']);
Route::get('completecount',[TodoController::class, 'completeCount']);
Route::post('/',[TodoController::class, 'create']);
Route::put('/',[TodoController::class, 'editContent']);
Route::put('active',[TodoController::class, 'active']);
Route::get('active',[TodoController::class, 'getAllActive']);
Route::put('complete',[TodoController::class, 'complete']);
Route::get('complete',[TodoController::class, 'getAllComplete']);
Route::put('completeall',[TodoController::class, 'completeAll']);
Route::delete('deletecomplete',[TodoController::class, 'deleteComplete']);
Route::delete('/',[TodoController::class, 'deleteid']);
Route::get('all', [TodoController::class, 'allCount']);

