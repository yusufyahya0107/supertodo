<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Todo = Todo::select("*")->orderBy('created_at', 'desc')->get();

        return ["msg"=> "success",
                "data"=> $Todo];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        //
        $req->validate([
            "content"=>["required"],
            "active"=>["required", "boolean"]
        ]);

        $Todo = new Todo;
        $Todo->content = $req->content;
        $Todo->active = $req->active;
        $Todo->save();
        return ["msg"=>"saved Succesfully",
                "content"=>$Todo->content,
                "active"=>$Todo->active,
                "id"=>$Todo->id];
    }
    // 1 active 0 complete

    public function editContent(Request $req){
        $req->validate([
            "id"=>["required"],
            "content"=>["required"]
        ]);
        $Todo = Todo::where('id', $req->id)->first();
        $Todo->content = $req->content;
        $Todo->save();
        return ["msg"=>"saved Succesfully",
        "content"=>$Todo->content,
        "active"=>$Todo->active,
        "id"=>$Todo->id];
    }

    public function active(Request $req){
        $req->validate([
            "id"=>["required"],
        ]);
        $Todo = Todo::where('id', $req->id)->first();
        
        if($Todo->active){
            return["msg"=>"data was data is already active"];
        }
        $Todo->active = true;
        $Todo->save();
        $isChange = $Todo->wasChanged();
        return ["msg"=>"data now active",
                "id"=>$Todo->id,
                "active"=>$Todo->active];
    }
    //put
    public function complete(Request $req){
        $req->validate([
            "id"=>["required"],
        ]);
        $Todo = Todo::where('id', $req->id)->first();
        if(!($Todo->active)){
            return["msg"=>"data was data is already complete"];
        }
        $Todo->active = false;
        $Todo->save();
        $isChange = $Todo->wasChanged();
        return ["msg"=>"data now Complete",
                "id"=>$Todo->id,
                "active"=>$Todo->active];
    }
    //put
    public function completeAll(){
        $Todo =Todo::where('active',true);
        $Todo->lazyById(200, $column = 'id')->each->update(['active' => false]);
        return ["msg"=>"updated successfully"];
    }
    //get
    public function getAllActive(){
        $Todo = Todo::select("*")->where('active',true)->orderBy('created_at', 'desc')->get();

        return ["msg"=> "success",
                "data"=> $Todo];
    }
    //get
    public function getAllComplete(){
        $Todo = Todo::select("*")->where('active',false)->orderBy('created_at', 'desc')->get();

        return ["msg"=> "success",
                "data"=> $Todo];
    }
    // get
    public function activeCount(){
        $Todo = Todo::where('active',true)->count();
        return["msg"=>"success",
                "count"=>$Todo];
    }
    // get
    public function completeCount(){
        $Todo = Todo::where('active',false)->count();
        return["msg"=>"success",
                "count"=>$Todo];
    }
    // get
    public function allCount(){
        $Todo = Todo::count();
        return["msg"=>"success",
                "count"=>$Todo];
    }
    // delete
    public function deleteComplete(){
        $Todo =Todo::where('active',false);
        $Todo->lazyById(200, $column = 'id')->each->delete();
        return ["msg"=>"suceess"];
    }

    // delete id based
    public function deleteid(Request $req){
        $req->validate(["id"=>["required"]]);
        $Todo =Todo::where('id', $req->id)->first();
        // if(count($Todo)){
        //     return ["msg"=> "Not Found"];
        // }
        $Todo->delete();
        return ["msg"=>"suceess"];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        //
    }
}
